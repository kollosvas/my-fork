from typing import List
from datetime import datetime

def deadline_score(pass_date: str, deadline_date: str) -> int:
    pass_day, pass_month, pass_year = map(int, pass_date.split('.'))
    deadline_day, deadline_month, deadline_year = map(int, deadline_date.split('.'))

    pass_datetime = datetime(pass_year, pass_month, pass_day)
    deadline_datetime = datetime(deadline_year, deadline_month, deadline_day)

    if pass_datetime <= deadline_datetime:
        return 5
    else:
        weeks_late = (pass_datetime - deadline_datetime).days // 7
        score = 5 - weeks_late

    if score < 0:
        return 0
    else:
        return score

def late_list(grades: dict, deadline_date: str) -> List[str]:
    late_students = []

    for student, pass_date in grades.items():
        if deadline_score(pass_date, deadline_date) < 5:
            late_students.append(student)
    return sorted(late_students)

def validate_date(date_string):
    try:
        datetime.strptime(date_string, "%d.%m.%Y")
        return date_string
    except ValueError:
        date_string = input("Введите дату заново в формате dd.mm.yyyy: ")
        return date_string


if __name__ == '__main__':
    grades_student = {}
    while True:
        way = (input("1 - узнать баллы, 2 - узнать опоздавших учеников, 'b' - выход: "))

        if way == 'b':
            break

        if way == '1':
            pass_date = str(input("Введите дату сдачи работы: "))
            pass_date = validate_date(pass_date)
            deadline_date = str(input("Введите дату окончания приёма работы: "))
            deadline_date = validate_date(deadline_date)
            print("Студент получит: ", deadline_score(pass_date, deadline_date))
        
        if way == '2':
            while True:
                student = input("Фамилия (или 'q' для завершения ввода студентов): ")
                if student == 'q':
                    break
                
                pass_date = input("Введите дату сдачи работы в формате dd.mm.yyyy: ")
                pass_date = validate_date(pass_date)
                grades_student[student] = pass_date
            deadline_date = input("Введите дату окончания приёма работы в формате dd.mm.yyyy: ")
            grades_student = late_list(grades_student, deadline_date)
            print(grades_student)

            print("Опоздавшие ученики: ")
        
    grades_student.clear()